/**
 * @class 			MapView
 * 
 * @author 			John Salis <jsalis@stetson.edu>
 * @description 	Builds geometry to represent map data and controls the adding and removing 
 *               	of map objects to the scene.
 */
MapView = (function() {

	/**
	 * @constant {Integer} 	The Y scale of the map walls in relation to the X Z scale.
	 */
	var HEIGHT_SCALE = 1.2;

	/**
	 * @constant {Integer} 	The decimal precision to use when comparing vertex positions.
	 */
	var PRECISION = 1 / Math.pow(10, 2);

	/**
	 * @constant {Integer} 	The number of steps it takes for the map to change visibility.
	 */
	var ANIMATE_VISIBLE_LENGTH = 30;

	/**
	 * @var {Boolean} 	Whether resources needs to be loaded using MapView.loadResources().
	 */
	var needsLoad = true;

	/**
	 * @var {Object}	Contains all geometry, textures and data shared by all MapView instances.
	 */
	var resource = {};

	/**
	 * Class Constructor
	 * 
	 * @param {THREE.Object3D} 	scene 	The container for all view objects.
	 * @param {Array} 			map   	A 2D array of map data.
	 * @param {THREE.Vector3} 	pos     The center position.
	 * @param {String} 			seed  	The seed to be used for random generation.
	 */
	function MapView(scene, map, pos, seed)
	{
		if (needsLoad)
		{
			console.error("MapView resources not loaded.");
		}

		pos.y *= HEIGHT_SCALE;

		this._visible = true;
		this._visibleTick = 0;

		this._scene = scene;
		this._seed = seed;

		this._container = new THREE.Object3D();
		this._container.position.set(pos.x, pos.y, pos.z);

		this._floorMaterial = resource.floorMaterial.clone();
		this._wallMaterial = resource.wallMaterial.clone();

		// CEILING
		this._ceilingMesh = createCeilingMesh(map, resource.ceilingMaterial);
		this._ceilingMesh.position.y = resource.mapHeight;
		this._container.add(this._ceilingMesh);

		// FLOOR
		this._floorMesh = createFloorMesh(map, this._floorMaterial);
		this._floorMesh.position.y = PRECISION;
		this._container.add(this._floorMesh);

		// WALL
		this._wallMesh = createWallMesh(map, this._wallMaterial);
		this._wallMesh.position.y = (resource.mapHeight / 2);
		this._container.add(this._wallMesh);

		this._scene.add(this._container);
	}

	/**
	 * Creates a new floor mesh using a given set of map data.
	 * 
	 * @param  {Array} 			map 		A 2D array of map data.
	 * @param  {THREE.Material} material 	The material to apply to the floor mesh.
	 * @return {THREE.Mesh}     			The floor mesh.
	 */
	function createFloorMesh(map, material)
	{
		var mesh = new THREE.Mesh(resource.floorGeometry, material);
		mesh.rotation.x = Math.PI / -2;

		return mesh;
	}

	/**
	 * Creates a new ceiling mesh using a given set of map data.
	 * 
	 * @param  {Array} 			map 		A 2D array of map data.
	 * @param  {THREE.Material} material 	The material to apply to the ceiling mesh.
	 * @return {THREE.Mesh}     			The ceiling mesh.
	 */
	function createCeilingMesh(map, material)
	{
		var geometry = new PartitionGeometry(resource.mapLength, resource.size, map);
		geometry.dynamic = false;

		var mesh = new THREE.Mesh(geometry, material);
		mesh.rotation.x = Math.PI / -2;

		return mesh;
	}

	/**
	 * Creates a chunk mesh for a section of the map data. The mesh is positioned according to the 
	 * position of the chunk in the map data. No special transformations are applied to the geometry.
	 * 
	 * @param  {Array} 			map 		A 2D array of map data.
	 * @param  {THREE.Material} material 	The material to apply to the wall mesh.
	 * @return {THREE.Mesh}      			The generated chunk mesh.
	 */
	function createWallMesh(map, material)
	{
		var wallGeometry = new THREE.Geometry();

		for (var i = 0; i < resource.size; i++)
		{
			for (var j = 0; j < resource.size; j++)
			{
				var block = new BlockView(map, i, j);

				if (block.geometry == null)
					continue;

				var bz = i + (1 / 2) - (resource.size / 2);
				var bx = j + (1 / 2) - (resource.size / 2);

				var transMatrix = new THREE.Matrix4().makeTranslation(bx, 0, bz);
				
				block.matrix.multiplyMatrices(transMatrix, block.matrix);
				wallGeometry.merge(block.geometry, block.matrix);
			}
		}

		wallGeometry.mergeVertices();
		wallGeometry.computeVertexNormals();
		wallGeometry.computeBoundingBox();
		wallGeometry.dynamic = false;

		var wallMesh = new THREE.Mesh(wallGeometry, material);

		wallMesh.scale.set(resource.scale, resource.mapHeight, resource.scale);
		wallMesh.updateMatrix();

		return wallMesh;
	}

	/**
	 * [loadTexture description]
	 * 
	 * @param  {[type]} path [description]
	 * @param  {[type]} rx   [description]
	 * @param  {[type]} ry   [description]
	 * @return {[type]}      [description]
	 */
	function loadTexture(path, rx, ry)
	{
	    var texture = THREE.ImageUtils.loadTexture(path);
	    texture.wrapS = THREE.RepeatWrapping;
	    texture.wrapT = THREE.RepeatWrapping;
	    texture.repeat.x = rx;
	    texture.repeat.y = ry;
	    return texture;
	}

	/**
	 * Loads resources to be shared and used by all instances of MapView. Resources include textures, materials, 
	 * and geometry. This function must be called before any instances are created.
	 * 
	 * @param  {Number} 	size  	The size of the map in units. Equal to the length of the map data.
	 * @param  {Number} 	scale 	The scale of the map.
	 */
	MapView.loadResources = function(size, scale)
	{
		resource.size = size;
		resource.scale = scale;
		resource.mapLength = size * scale;
		resource.mapHeight = scale * HEIGHT_SCALE;

		// WIRE MATERIAL
		resource.wireMaterial = new THREE.MeshBasicMaterial({ 
			color: 0xff0000,
			wireframe: true
		});

		// GREY MATERIAL
		resource.greyMaterial = new THREE.MeshBasicMaterial({
			color: 0x444F58,
			vertexColors: THREE.VertexColors,
			shading: THREE.SmoothShading
		});

		// HIDDEN MATERIAL
		resource.hiddenMaterial = new THREE.MeshBasicMaterial({
			visible: false
		});

		// FLOOR MATERIAL
		var floorTexture = loadTexture("_img/floor.jpg", size, size);
		resource.floorMaterial = new THREE.MeshLambertMaterial({
			transparent: true,
			map: floorTexture,
			side: THREE.DoubleSide,
		});

		// FLOOR GEOMETRY
		resource.floorGeometry = new THREE.PlaneGeometry(resource.mapLength, resource.mapLength, size, size);

		// WALL MATERIAL
		var wallTexture = loadTexture("_img/wall.jpg", 1, HEIGHT_SCALE);
		resource.wallMaterial = new THREE.MeshLambertMaterial({
			transparent: true,
			map: wallTexture,
			side: THREE.DoubleSide,
			vertexColors: THREE.VertexColors,
			shading: THREE.SmoothShading,
		});

		// CEILING MATERIAL
		var materialList = [];
		materialList[ Map.WALL ] = resource.greyMaterial;
		materialList[ Map.FLOOR ] = resource.hiddenMaterial;

		resource.ceilingMaterial = new THREE.MeshFaceMaterial(materialList);

		needsLoad = false;
	};

	MapView.prototype = {

		constructor: MapView,

		/**
		 * Gets a reference to the mesh representing the wall boundary.
		 * 
		 * @return {THREE.Mesh} 		The wall boundary mesh.
		 */
		getBoundary: function()
		{
			return this._boundaryMesh;
		},

		/**
		 * Gets a reference to the ceiling geometry. Essentially, a 2D representation of the map view structure.
		 * 
		 * @return {THREE.Geometry} 	The ceiling geometry.
		 */
		getCeilingGeometry: function()
		{
			return this._ceilingMesh.geometry;
		},

		/**
		 * Sets the target visibility of the map view and optionally starts the animation.
		 * 
		 * @param {Boolean} visible
		 * @param {Boolean} animate
		 */
		setVisible: function(visible, animate)
		{
			var animate = (animate !== undefined) ? animate : true;

			if (this._visible != visible)
			{
				this._visible = visible;

				if (animate)
				{
					this._visibleTick = ANIMATE_VISIBLE_LENGTH;
				}
				else
				{
					for (var i = 0; i < this._container.children.length; i++)
					{
						this._container.children[i].material.opacity = (visible) ? 1 : 0;
					}
				}
			}
		},

		/**
		 * Updates the map view.
		 */
		run: function()
		{
			if (this._visibleTick > 0)
			{
				var step = (1 / ANIMATE_VISIBLE_LENGTH);

				if (this._visible)
				{
					this._floorMaterial.opacity += step;
					this._floorMaterial.opacity = Math.min(this._floorMaterial.opacity, 1);

					this._wallMaterial.opacity += step;
					this._wallMaterial.opacity = Math.min(this._wallMaterial.opacity, 1);
				}
				else
				{
					this._floorMaterial.opacity -= step;
					this._floorMaterial.opacity = Math.max(this._floorMaterial.opacity, 0);

					this._wallMaterial.opacity -= step;
					this._wallMaterial.opacity = Math.max(this._wallMaterial.opacity, 0);
				}

				this._visibleTick --;
			}
		},

		/**
		 * Removes all objects from the scene and disposes geometry.
		 */
		destruct: function()
		{
			this._scene.remove(this._container);
			
			this._ceilingMesh.geometry.dispose();
			this._wallMesh.geometry.dispose();

			this._floorMaterial.dispose();
			this._wallMaterial.dispose();
		},
	};

	return MapView;

})();
