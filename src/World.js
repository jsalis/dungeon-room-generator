/**
 * Class:       World
 * Author:      John Salis
 * Purpose:     Stores a 2D array of map objects and
 *				keeps track which maps are generated.
 */

World.prototype.constructor = World;

World.MAP_SCALE = 2;
World.MAP_SIZE = 64;

function World(mapSize, mapScale)
{
	var mapList = [];
	var currentMapIndex = 0;

	generateArea(currentMapIndex);

	function isMapAvailable(mapIndex)
	{
		return (mapList[mapIndex] !== undefined && mapList[mapIndex].data !== null);
	}

	function generateArea(mapIndex)
	{
		for (var i = 0; i <= 0; i++)  // -1 to 1
		{
			if (mapList[mapIndex + i] === undefined)
			{
				mapList[mapIndex + i] = new Map(mapSize);
			}

			if (mapList[mapIndex + i].data === null)
			{
				mapList[mapIndex + i].generate();
			}
		}
	}

	function clearArea(mapIndex)
	{
		for (var i = -1; i <= 1; i++)
		{
			mapList[mapIndex + i].clear();
		}
	}

	this.getCurrentMapIndex = function()
	{
		return currentMapIndex;
	};

	this.getMapPosition = function(mapIndex)
	{
		return new THREE.Vector3(0, mapIndex * mapScale, 0);
	};

	this.getCellPosition = function(mapIndex, cellRow, cellCol)
	{
		var mapLength = mapSize * mapScale;
		var y = mapIndex * mapScale;
		var z = (cellRow * mapScale) + (mapScale / 2) - (mapLength / 2);
		var x = (cellCol * mapScale) + (mapScale / 2) - (mapLength / 2);
		return { z : z, y : y, x : x };
	};

	this.getCellValue = function(mapIndex, cellIndex)
	{
		return mapList[mapIndex].getCellValue(cellIndex);
	};

	this.getRandomCellPosition = function(mapIndex, cellType)
	{
	    var position = Generator.getRandomPosition(mapList[mapIndex].data, cellType);
		return (position === null) ? null : this.getCellPosition(position.row, position.col);
	};

	this.getMapData = function(mapIndex)
	{
		var data = mapList[mapIndex].data;
		var temp = [];

		for (var i = 0; i < data.length; i++)
		{
			temp[i] = [];
			for (var j = 0; j < data[i].length; j++)
			{
				temp[i][j] = data[i][j];
			}
		}

		temp[-1] = [];
		temp[data.length] = [];

		return temp;
	};

	this.getMapSeed = function(mapIndex)
	{
		return mapList[mapIndex].seed;
	};

	this.getMapSize = function()
	{
		return mapSize;
	};

	this.getMapScale = function()
	{
		return mapScale;
	};

	this.moveUp = function()
	{
		mapList[currentMapIndex - 1].clear();
		currentMapIndex ++;
		generateArea(currentMapIndex);
	};

	this.moveDown = function()
	{
		mapList[currentMapIndex + 1].clear();
		currentMapIndex --;
		generateArea(currentMapIndex);
	};
}
