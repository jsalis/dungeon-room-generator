/**
 * @class           GameModel
 * 
 * @author          John Salis
 * @description     Model containing all game related objects.
 */
GameModel = (function() {

    // Declare constructor
    GameModel.prototype.constructor = GameModel;

    /**
     * Constructor
     */
    function GameModel(renderer)
    {
        this.renderer = renderer;

        this.tick = 0;

        this.scene = new GameScene();
        this.scene.camera = createCamera();
        this.scene.add(this.scene.camera);

        // Create input manager
        this.inputManager = new InputManager();
        this.inputManager.mapKey(action.MOVE_FORWARD, 87);
        this.inputManager.mapKey(action.MOVE_BACKWARD, 83);
        this.inputManager.mapKey(action.MOVE_LEFT, 65);
        this.inputManager.mapKey(action.MOVE_RIGHT, 68);

        // Create HUD
        this.headsUpDisplay = new HeadsUpDisplay(this.renderer);

        // Create world controller
        this.worldController = new WorldController(this.scene);
    }

    /**
     * Creates and returns a new camera. Also sets up a controller for the camera.
     * 
     * @return {THREE.Camera}
     */
    function createCamera()
    {
        var aspect = window.innerWidth / window.innerHeight;
        var fov = 45;
        var frustumNear = 1;
        var frustumFar = World.MAP_SIZE * 4;
        var camera = new THREE.PerspectiveCamera(fov, aspect, frustumNear, frustumFar);

        // Create a controller for the camera
        var cameraController = new THREE.OrbitControls(camera);
        cameraController.minDistance = 8;
        cameraController.maxDistance = World.MAP_SIZE * 2;
        cameraController.maxPolarAngle = Math.PI / 2.2;
        cameraController.target = new THREE.Vector3(0, 0, 0);

        // Initialize camera position
        camera.position.x = 0;
        camera.position.y = cameraController.maxDistance / 2;
        camera.position.z = cameraController.maxDistance / 2;
        cameraController.update();

        return camera;
    }

    /**
     * Renders the scene, and updates all game objects.
     */
    GameModel.prototype.run = function()
    {
        // if (this.tick % 120 == 0 && this.tick > 120)
        // {
        //     this.worldController.moveUp();
        // }

        // Update the world controller
        this.worldController.run();

        // Render the scene
        this.renderer.render(this.scene, this.scene.camera);

        // Render the HUD
        this.headsUpDisplay.render();

        // Add a tick
        this.tick ++;
    };

    return GameModel;

})();
