/**
 * Class:       InputManager
 * Author:      John Salis
 * Purpose:     Keeps track of which keys are pressed
 * 
 * Date:        November 6, 2013
 */
 
var action = {
   MOVE_FORWARD:  	0,
   MOVE_BACKWARD: 	1,
   MOVE_LEFT:   	2,
   MOVE_RIGHT:  	3,
   TURN_LEFT:     	4,
   TURN_RIGHT:    	5,
   MOVE_UP:       	6,
   MOVE_DOWN:     	7,
   FIRE:          	8,
   ONE:             9,
   TWO:             10,
   THREE:           11,
   FOUR:            12,
   MAX_INDEX:     	13,
};

InputManager.prototype.constructor = InputManager;

function InputManager()
{
    var keyMap = new Array(action.MAX_INDEX);
    var isDown = new Array(action.MAX_INDEX);
    for (var i = 0; i < isDown.length; i++)
    {
        isDown[i] = false;
    }
    document.addEventListener("keydown", onKeyDown, false);
    document.addEventListener("keyup", onKeyUp, false);


    function onKeyDown(event)
	{
		var keyCode = event.which;
	    for (var i = 0; i < keyMap.length; i++)
	    {
	        if (keyMap[i] == keyCode)
	        {
	            isDown[i] = true;
	            return;
	        }
	    }
	}


	function onKeyUp(event)
	{
	    var keyCode = event.which;
	    for (var i = 0; i < keyMap.length; i++)
	    {
	        if (keyMap[i] == keyCode)
	        {
	            isDown[i] = false;
	            return;
	        }
	    }
	}


    this.down = function(action)
	{
	    return isDown[action];
	};


	this.mapKey = function(action, keyCode)
	{
	    if (action >= 0 && action < keyMap.length)
	    {
	        keyMap[action] = keyCode;
	        return true;
	    }
	    else return false;
	};
}
