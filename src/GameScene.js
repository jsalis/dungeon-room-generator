/**
 * @class           GameScene
 * 
 * @author          John Salis
 * @description     Extension of THREE.Scene that holds all renderable game objects.
 */
GameScene = (function() {

    // Extend from THREE.Scene
    GameScene.prototype = Object.create(THREE.Scene.prototype);

    // Declare constructor
    GameScene.prototype.constructor = GameScene;

    /**
     * Constructor
     */
    function GameScene()
    {
        // Call the parent constructor
        THREE.Scene.call(this);

        this.camera = null;

        // Create skybox
        var radius = World.MAP_SIZE * 2;
        var skyGeometry = new THREE.SphereGeometry(radius);
        var skyMaterial = new THREE.MeshBasicMaterial({ color: 0xffffff, side: THREE.BackSide });
        var skyMesh = new THREE.Mesh(skyGeometry, skyMaterial);
        this.add(skyMesh);
        
        // Create hemisphere light
        var hemiLight = new THREE.HemisphereLight(0xffffff, 0xffffff, 1);
        this.add(hemiLight);

        // Create directional light
        var directionalLight = new THREE.DirectionalLight(0xffffff, 0.5);
        directionalLight.position.set(0.5, 1, 1);
        this.add(directionalLight);
    }

    return GameScene;

})();
