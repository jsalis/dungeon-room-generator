
$(document).ready(function() {

    var renderer = createRenderer('webGL');
    var stats = createStats('stats');

    var gameModel = new GameModel(renderer);

    window.addEventListener('resize', onWindowResize, false);

    initControlPanel();
    run();

    function createRenderer(containerId)
    {
        // Get container
        container = document.getElementById(containerId);

        // Create renderer and add it to the container
        var renderer = new THREE.WebGLRenderer({ antialias: false });
        renderer.setSize(window.innerWidth, window.innerHeight);
        container.appendChild(renderer.domElement);

        return renderer;
    }

    function createStats(containerId)
    {
        var stats = new Stats();
        stats.setMode(0); // 0: fps, 1: ms

        // Align top-left
        stats.domElement.style.position = 'absolute';
        stats.domElement.style.left = '0px';
        stats.domElement.style.top = '0px';

        $('#' + containerId).append(stats.domElement);

        return stats;
    }

    function onWindowResize()
    {
        gameModel.scene.camera.aspect = window.innerWidth / window.innerHeight;
        gameModel.scene.camera.updateProjectionMatrix();
        renderer.setSize(window.innerWidth, window.innerHeight);
    }

    function initControlPanel()
    {
        this.controls = new function() {
            // TO DO
        };

        // Initialize a new GUI.
        // this.gui = new dat.GUI({ width: 300 });
    }

    function run()
    {
        // Update game model
        gameModel.run();

        // Update performance stats
        stats.update();
        
        // Request another frame
        requestAnimationFrame(run);
    }
});
