/**
 * Class:       Map
 * Author:      John Salis
 */

Map.WALL = 0;
Map.FLOOR = 1;

Map.prototype.constructor = Map;

function Map(length)
{
	this.length = length;
	this.data = null;
	this.seed = Generator.getSeed();
}


Map.prototype.generate = function()
{
	var gen = (Math.random() < 0.5) ? BasicGenerator.generate : DungeonGenerator.generate;
	this.data = gen(this.length, Map.WALL, Map.FLOOR);
};


Map.prototype.getElement = function(row, col)
{
	return (this.data === null) ? null : this.data[row][col];
};


Map.prototype.setElement = function(row, col, val)
{
	if (this.data !== null)
	{
		this.data[row][col] = val;
	}
};


Map.prototype.clear = function()
{
	this.data = null;
};


Map.onMap = function(map, i)
{
	if (i < 0)
	{
		return -1;
	}
	else if (i >= map.length)
	{
		return 1;
	}
	else return 0;
};


Map.prototype.getCellCount = function()
{
	return this.length * this.length;
};


Map.prototype.getIndex = function(row, col)
{
	if (this.onMap(row, col))
	{
		return (row * this.length) + col;
	}
	else 
		return -1;
};


Map.prototype.getRowCol = function(i)
{
	var r = Math.floor(i / this.length);
	var c = i % this.length;
	return { row: r, col: c };
};


Map.prototype.getCellValue = function(i)
{
	var pos = this.getRowCol(i);
	if (this.onMap(pos.row, pos.col))
	{
		return this.data[pos.row][pos.col];
	}
	else 
		return -1;
};


Map.prototype.print = function()
{
	if (this.data != null)
	{
		Generator.print(this.data);
	}
};
