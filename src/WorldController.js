/**
 * Class:       WorldController
 * Author:      John Salis
 */

WorldController.prototype.constructor = WorldController;

function WorldController(scene)
{
	var world = new World(World.MAP_SIZE, World.MAP_SCALE);
    var worldView = new WorldView(scene, world);

    this.run = function()
    {
    	worldView.run();
    };

    this.addMoveable = function(moveable)
	{
		worldView.addMoveable(moveable);
	};

    this.getLocalPosition = function(x, z)
    {
    	var mapIndex = world.getCurrentMapIndex();
		var pos = world.getMapPosition(mapIndex);
		return { x : x - pos.x, z : z - pos.z };
    };

    this.getMapPosition = function(mapIndex)
    {
    	return world.getMapPosition(mapIndex);
    };

    this.getCellPosition = function(mapIndex, cellRow, cellCol)
    {
    	return world.getCellPosition(mapIndex, cellRow, cellCol);
    };

    this.getRandomCellPosition = function(mapIndex, cellType)
    {
	    return world.getRandomCellPosition(mapIndex, cellType);
    };

    this.moveUp = function()
    {
        world.moveUp();
        worldView.moveUp();
    };

    this.moveDown = function()
    {
        world.moveDown();
        worldView.moveDown();
    };
}
