/**
 * Class:       WorldView
 * Author:      John Salis
 * Purpose:     Builds geometry to represent map data and
 *				controls the adding and removing of blocks 
 *				to the scene.
 */

WorldView.prototype.constructor = WorldView;

function WorldView(scene, world)
{
	MapView.loadResources(world.getMapSize(), world.getMapScale());

	var moveableList = [];
	var mapView = [];

	createMap(0);

	// for (var i = -1; i <= 1; i++)
	// {
	// 	createMap(i);
	// }

	function createMap(i)
	{
		var mapIndex = world.getCurrentMapIndex();
		var map = world.getMapData(mapIndex + i);

		// Create a new map view
		var pos = world.getMapPosition(mapIndex + i);
		var seed = world.getMapSeed(mapIndex + i);
		
		mapView[i] = new MapView(scene, map, pos, seed);
		mapView[i].setVisible(i == 0, false);

		var o = mapView[i].getBoundary();
		for (var n = 0; n < moveableList.length; n++)
		{
			moveableList[n].addObstacle(o);
		}
	}

	this.addMoveable = function(moveable)
	{
		moveableList.push(moveable);
		for (var i = -1; i <= 1; i++)
		{
			var o = mapView[i].getBoundary();
			moveable.addObstacle(o);
		}
	};

	this.run = function()
	{
		// for (var i = -1; i <= 1; i++)
		// {
		// 	mapView[i].run();
		// }
	};

	this.moveUp = function()
	{
		// mapView[-1].destruct();
		// // remove obstacle ?
		
		// mapView[-1] = mapView[0];
		// mapView[-1].setVisible(false);

		// mapView[0] = mapView[1];
		// mapView[0].setVisible(true);

		// createMap(1);
	};

	this.moveDown = function()
	{
		// mapView[1].destruct();
		// // remove obstacle ?
		
		// mapView[1] = mapView[0];
		// mapView[1].setVisible(false);

		// mapView[0] = mapView[-1];
		// mapView[0].setVisible(true);

		// createMap(-1);
	};
}
