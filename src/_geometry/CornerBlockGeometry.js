/**
 * Class:       CornerBlockGeometry
 * Author:      John Salis
 * Purpose:     
 * 
 * Date:        May 13, 2014
 */

CornerBlockGeometry.prototype = new THREE.PlaneGeometry();

CornerBlockGeometry.prototype.constructor = CornerBlockGeometry;

function CornerBlockGeometry(size, height, widthSegments, heightSegments)
{
	THREE.PlaneGeometry.call(this, size * Math.sqrt(2), height, widthSegments, heightSegments);

	var axis = new THREE.Vector3(0, 1, 0);
	var angle = Math.PI / 4;
	var matrix = new THREE.Matrix4().makeRotationAxis(axis, angle);

	for (var i = 0; i < this.vertices.length; i++)
	{
		this.vertices[i].applyMatrix4(matrix);
	}
	
	this.computeFaceNormals();
	this.computeVertexNormals();
}
