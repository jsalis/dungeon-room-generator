/**
 * Class:       SideBlockGeometry
 * Author:      John Salis
 * Purpose:     
 * 
 * Date:        May 13, 2014
 */

SideBlockGeometry.prototype = new THREE.PlaneGeometry();

SideBlockGeometry.prototype.constructor = SideBlockGeometry;

function SideBlockGeometry(size, height, widthSegments, heightSegments)
{
	THREE.PlaneGeometry.call(this, size, height, widthSegments, heightSegments);

	var matrix = new THREE.Matrix4().makeTranslation(0, 0, size / 2);

	for (var i = 0; i < this.vertices.length; i++)
	{
		this.vertices[i].applyMatrix4(matrix);
	}

	this.computeFaceNormals();
	this.computeVertexNormals();
}
