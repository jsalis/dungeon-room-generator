
DiamondGeometry.prototype = new THREE.Geometry();

DiamondGeometry.prototype.constructor = DiamondGeometry;

function DiamondGeometry(width, height, widthSegments, heightSegments)
{
    THREE.Geometry.call(this);

    this.width = width;
    this.height = height;

    this.widthSegments = widthSegments || 1;
    this.heightSegments = heightSegments || 2;

    var long_row = this.widthSegments + 1;
    var short_row = this.widthSegments;

    // First Row is the long_row, the ternary statement will toggle this
    var current_row = short_row;
    var gridY = 0;
    var vX = width / 2, vY = height / 2;

    var ix, iz;
    var width_half = width / 2;
    var height_half = height / 2;

    var gridX = this.widthSegments;
    var gridZ = this.heightSegments;

    var gridX1 = gridX + 1;
    var gridZ1 = gridZ + ( gridZ - 2) + 1;

    var segment_width = this.width / gridX;
    var segment_height = this.height / gridZ;

    var normal = new THREE.Vector3( 0, 0, 1 );

    // Height Segments Verticies
    for ( iz = 0; iz < (gridZ1 + 1) * 2; iz ++ ) 
    {
        // Ternary Operator:
        current_row === long_row ? (current_row = short_row, vX = width_half - (segment_width / 2) ) : (current_row = long_row, vX = width_half);

        // Width Segment Verticies
        for ( ix = 0; ix < current_row; ix ++ )
        {
            var x = ix * segment_width - vX ;
            var y = (iz * segment_height - vY) / 2 - (vY / 2);

            this.vertices.push( new THREE.Vector3( x, - y, 0 ) );
        }
    }

    var uva = new THREE.Vector2(0, 1);
    var uvb = new THREE.Vector2(1, 1);
    var uvc = new THREE.Vector2(0.5, 0.5);
    var uvd = new THREE.Vector2(0, 0);
    var uve = new THREE.Vector2(1, 0);

    for ( iz = 0; iz < gridZ ; iz ++ )
    {
            for ( ix = 0; ix < gridX; ix ++ )
            {
                var a = ix + gridX * iz + (iz * gridX1) ;
                var b = a + 1;
                var c = a + gridX1;
                var d = c + gridX;
                var e = d + 1;

                // UP
                var face = new THREE.Face3( c, b, a );
                face.normal.copy( normal );
                face.vertexNormals.push( normal, normal, normal );

                this.faces.push( face );
                this.faceVertexUvs[ 0 ].push( [ uvc, uvb, uva ] );

                // DOWN
                face = new THREE.Face3( e, c, d );
                face.normal.copy( normal );
                face.vertexNormals.push( normal, normal, normal );

                this.faces.push( face );
                this.faceVertexUvs[ 0 ].push( [ uve, uvc, uvd ] );

                // LEFT
                face = new THREE.Face3( d, c, a );
                face.normal.copy( normal );
                face.vertexNormals.push( normal, normal, normal );

                this.faces.push( face );
                this.faceVertexUvs[ 0 ].push( [ uvd, uvc, uva ] );

                // RIGHT
                face = new THREE.Face3( e, b, c );
                face.normal.copy( normal );
                face.vertexNormals.push( normal, normal, normal );

                this.faces.push( face );
                this.faceVertexUvs[ 0 ].push( [ uve, uvb, uvc ] );
            }
    }
}
