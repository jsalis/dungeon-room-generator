/**
 * Class:       PartitionGeometry
 * Author:      John Salis
 * Purpose:     Extension of a plane geometry that uses an array of data
 *				to hide a set of faces and color a set of vertices
 * 
 * Date:        May 16, 2014
 */

PartitionGeometry.prototype = new DiamondGeometry();

PartitionGeometry.prototype.constructor = PartitionGeometry;

function PartitionGeometry(length, size, data)
{
	DiamondGeometry.call(this, length, length, size, size);

	var MAX_NEIGHBORS = 8;

	var black = new THREE.Color(0x000000);
	var fade = new THREE.Color(0x777777);
	var clear = new THREE.Color(0xffffff);
	var faceIndices = ['a','b','c'];

	var index = 0;
	for (var i = 0; i < size; i++)
	{
		for (var j = 0; j < size; j++)
		{
			// Check current cell and neighboring cells
			var center = data[i][j] != Map.FLOOR;
			var north = data[i - 1][j] != Map.FLOOR;
			var south = data[i + 1][j] != Map.FLOOR;
			var west = data[i][j - 1] != Map.FLOOR;
			var east = data[i][j + 1] != Map.FLOOR;

			// Set flags for the four possible tip configurations
			var nn = north && !west && !south && !east;
			var ss = !north && !west && south && !east;
			var ww = !north && west && !south && !east;
			var ee = !north && !west && !south && east;

			var tip = (nn || ss || ww || ee) && center;

			// Set flags for the four possible corner configurations
			var nw = north && west && !south && !east;
			var ne = north && east && !south && !west;
			var sw = south && west && !north && !east;
			var se = south && east && !north && !west;

			var corner = (nw || ne || sw || se) && center;

			// Count total neighbors (excludes center)
			var neighborCount = 0;
			for (var ii = -1; ii <= 1; ii++)
			{
				for (var jj = -1; jj <= 1; jj++)
				{
					if (data[i + ii][j + jj] != Map.FLOOR && (ii != 0 || jj != 0))
					{
						neighborCount ++;
					}
				}
			}

			var cell = index * 4;

			// Set face material
			if (tip)
			{
				this.faces[cell].materialIndex = (nn) ? data[i][j] : Map.FLOOR;
				this.faces[cell + 1].materialIndex = (ss) ? data[i][j] : Map.FLOOR;
				this.faces[cell + 2].materialIndex = (ww) ? data[i][j] : Map.FLOOR;
				this.faces[cell + 3].materialIndex = (ee) ? data[i][j] : Map.FLOOR;
			}
			else if (corner)
			{
				this.faces[cell].materialIndex = (nw || ne) ? data[i][j] : Map.FLOOR;
				this.faces[cell + 1].materialIndex = (sw || se) ? data[i][j] : Map.FLOOR;
				this.faces[cell + 2].materialIndex = (nw || sw) ? data[i][j] : Map.FLOOR;
				this.faces[cell + 3].materialIndex = (ne || se) ? data[i][j] : Map.FLOOR;
			}
			else
			{
				this.faces[cell].materialIndex = data[i][j];
				this.faces[cell + 1].materialIndex = data[i][j];
				this.faces[cell + 2].materialIndex = data[i][j];
				this.faces[cell + 3].materialIndex = data[i][j];
			}

			var clearList = new Array();
			var vCenter = ((size * 2 + 1) * i) + size + 1 + j;

			// Determine which neighboring vertices to color clear (not black)
			if (corner) clearList.push(vCenter);
			if ((corner && !nw) || !north || !west || data[i - 1][j - 1] == Map.FLOOR) clearList.push(vCenter - size - 1);	// Push northwest vertex
			if ((corner && !ne) || !north || !east || data[i - 1][j + 1] == Map.FLOOR) clearList.push(vCenter - size);		// Push northeast vertex
			if ((corner && !sw) || !south || !west || data[i + 1][j - 1] == Map.FLOOR) clearList.push(vCenter + size);		// Push southwest vertex
			if ((corner && !se) || !south || !east || data[i + 1][j + 1] == Map.FLOOR) clearList.push(vCenter + size + 1);	// Push southeast vertex

			// Determine if the center vertex needs to be faded between clear and black
			var fadeCenter = false;
			if (!corner && !tip)
			{
				if ((west ^ east) || (north ^ south) || neighborCount == MAX_NEIGHBORS - 2)
				{
					fadeCenter = true;
				}
				else if (neighborCount != MAX_NEIGHBORS - 1)
				{
					clearList.push(vCenter);
				}
			}

			// Color the vertices of each face
			for (var next = 0; next < 4; next++)
			{
				var face = this.faces[cell + next];
				for (var m = 0; m < faceIndices.length; m++)
				{
					var current = face[faceIndices[m]];
					if (face.materialIndex == Map.FLOOR || neighborCount == 0 || tip)
					{
						face.vertexColors[m] = clear;
					}
					else if (neighborCount == MAX_NEIGHBORS)
					{
						face.vertexColors[m] = black;
					}
					else if (current == vCenter && fadeCenter)
					{
						face.vertexColors[m] = fade;
					}
					else
					{
						face.vertexColors[m] = (clearList.indexOf(current) == -1) ? black : clear;
					}
				}
			}

			index ++;
		}
	}
}
