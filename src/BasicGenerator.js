/**
 * @class BasicGenerator
 * 
 * @author Nathan Hilliard <nhilliar@stetson.edu>
 * @description Static functions to modify a 2D array in a way that procedurally builds a dungeon.
 */
BasicGenerator = {};

// When generating points, make sure they're spaced out.
BasicGenerator.MIN_SEPARATION = 12;

// The target number of rooms.
BasicGenerator.TARGET_NUM_ROOMS = 12;

// Room size constants
BasicGenerator.MAX_SIZE = 4;
BasicGenerator.MIN_SIZE = 2;

// How many iterations since the last point was added until we give up.
BasicGenerator.TOLERANCE = 20;

BasicGenerator.generate = function(size, wall_value, floor_value)
{
	function drawPath(data, path)
	{
		for (var i = 0; i < path.length; i++)
		{
			var p = BasicGenerator.getRowCol(data, path[i]);
			data[p.row][p.col] = floor_value;
		}
	}

	function prim(data, list)
	{
		// Initialize our weight array
		var w = [];
		for (var i = 0; i < list.length; i++)
		{
			w[i] = [];
		}

		// Calculate the weights from each point to each other point
		for (var i = 0; i < list.length; i++)
		{
			var p1 = list[i];
			for (var j = 0; j < list.length; j++)
			{
				// If i == j then the vertex is being compared against itself meaning w = 0.
				if (i == j)
				{
					w[i][j] = 0;
					continue;
				}

				var p2 = list[j];

				w[i][j] = p1.distanceTo(p2);
			}
		}

		var visited = [];

		// Grab the first point
		var p = 0;
		visited.push(p);
		var minCost = Number.MAX_VALUE;
		var edge = {};

		for (var n = 0; n < list.length - 1; n++)
		{
			minCost = Number.MAX_VALUE;
			for (var i = 0; i < visited.length; i++)
			{
				for (var j = 0; j < list.length; j++)
				{
					if (visited.indexOf(j) != -1)
						continue;

					var v = visited[i];

					if (w[v][j] < minCost)
					{
						minCost = w[v][j];
						edge.x = v;
						edge.y = j;
					}
				}
			}
			
			var start = { row: list[edge.x].x, col: list[edge.x].y };
			var target = { row: list[edge.y].x, col: list[edge.y].y };

			// Grab the path
			var path = Generator.findPath(data, start, target);

			// Draw the path
			drawPath(data, path);

			visited.push(edge.y);
		}
	}

	// Initialize the data
	var data = [];

	for (var i = 0; i < size; i++)
	{
		data[i] = [];
		for (var j = 0; j < size; j++)
		{
			data[i][j] = wall_value;
		}
	}

	// Generate a list of points that will serve as a starting point for each room.
	var points = BasicGenerator.generatePoints(data, null, BasicGenerator.TARGET_NUM_ROOMS);

	// Initialize the RNG
	var r = Math.random; //(seed == null) ? Math.random : new Math.seedrandom(seed);

	for (var i = 0; i < points.length; i++)
	{
		var p = points[i];
		data[p.x][p.y] = floor_value;

		var range = BasicGenerator.MAX_SIZE - BasicGenerator.MIN_SIZE;

		var height = BasicGenerator.MIN_SIZE + Math.round(r() * range);
		var width = BasicGenerator.MIN_SIZE + Math.round(r() * range);

		// Loop through
		for (var x = (p.x - height); x <= (p.x + height); x++)
		{
			for (var y = (p.y - width); y <= (p.y + width); y++)
			{
				data[x][y] = floor_value;
			}
		}
	}

	prim(data, points);

	return data;
};

BasicGenerator.getRandomPosition = function(data, seed)
{
	// RNG
	var r = (seed === null) ? Math.random : new Math.seedrandom(seed);

	// Coords
	var x = Math.round(r() * (data.length - 1));
	var y = Math.round(r() * (data.length - 1));

	// Return the randomly picked coordinate.
	return new THREE.Vector2(x, y);
};

/**
 * Randomly generates a list of n coordinates that are at least
 * BasicGenerator.MIN_SEPARATION from each other.
 */
BasicGenerator.generatePoints = function(data, seed, n)
{
	var points = [];
	var ttl = 0;

	while (points.length < n && ttl < BasicGenerator.TOLERANCE)
	{
		var add = true;
		var p = BasicGenerator.getRandomPosition(data, seed);
		var half_sep = BasicGenerator.MIN_SEPARATION / 2;

		// If we aren't a certain distance to the walls, retry.
		if (p.x < half_sep || p.y < half_sep || (data.length - p.x) < half_sep || (data.length - p.y) < half_sep)
		{
			ttl ++;
			continue;
		}

		for (var i = 0; i < points.length; i++)
		{
			// If we aren't a certain distance to each other point, retry.
			if (p.distanceTo(points[i]) < BasicGenerator.MIN_SEPARATION)
			{
				add = false;
				ttl ++;
				break;
			}
		}

		// Push the point onto our array.
		if (add)
		{
			ttl = 0;
			points.push(p);
		}
	}

	return points;
};

BasicGenerator.getRowCol = function(data, index)
{
	var r = Math.floor(index / data[0].length);
	var c = index % data[0].length;
	return { row: r, col: c };
};
