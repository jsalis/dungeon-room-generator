/**
 * @class 			Generator
 * 
 * @author 			John Salis <jsalis@stetson.edu>
 * @description 	Holds static functions for generating and mutating two dimensional 
 *               	data sets using various algorithms.
 */
Generator = (function() {

	/**
	 * Class Constructor
	 */
	function Generator() {}

	/**
	 * Checks if the specified element exists within a 2D array of data.
	 * 
	 * @param  {Array}   data 	The 2D array of data.
	 * @param  {Number}  row    The row of the array to check.
	 * @param  {Number}  col    The column of the array to check.
	 * @return {Boolean}        Whether the element is not null.
	 */
	function elementExists(data, row, col)
	{
		return (data != null && data[row] != null && data[row][col] != null);
	}

	/**
	 * Compares the index of an array to the range of the data. Output is 0 if
	 * the index is within range, -1 if below the range, and 1 if above the range.
	 * 
	 * @param  {Array}  data 	The 2D array of data.
	 * @param  {Number} index   The index to compare.
	 * @return {Number}     	The result of the comparison. {-1, 0, 1}
	 */
	function checkIndex(data, index)
	{
		if (index < 0)
		{
			return -1;
		}
		else if (index >= data.length)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	/**
	 * Gets the index of a row and column in relation to a specified data set.
	 * 
	 * @param  {Array}  data  The 2D array of data.
	 * @param  {Number} row   The row of the element.
	 * @param  {Number} col   The column of the element.
	 * @return {Number}       The index. -1 if row and column are out of bounds.
	 */
	function getIndex(data, row, col)
	{
		if (checkIndex(data, row) == 0 && checkIndex(data, col) == 0)
		{
			return (row * data[0].length) + col;
		}
		else
		{
			return -1;
		}
	}

	/**
	 * Gets the row and column of index in relation to a specified data set.
	 * 
	 * @param  {Array}  data   The 2D array of data.
	 * @param  {Number} index  The index of the element.
	 * @return {Object}        Object containing the row and col as properties.
	 */
	function getRowCol(data, index)
	{
		var r = Math.floor(index / data[0].length);
		var c = index % data[0].length;
		return { row: r, col: c };
	}

	/**
	 * Gets a random value that can be used seed any random generation.
	 * 
	 * @return {String}
	 */
	Generator.getSeed = function()
	{
		return Math.floor(Math.random() * Number.MAX_VALUE).toString();
	};

	/**
	 * Finds the path based on start and target, later will also take in account dead spots and towers
	 * 
	 * @param  {Array}  data   [description]
	 * @param  {Object} start  current location
	 * @param  {Object} target destiantion
	 * @return {Array}  path   [description]
	 */
	Generator.findPath = function(data, start, target)
	{
		// Constants for cost
		var STRAIGHT_COST = 10;
		var DIAGONAL_COST = 14;

		// Count the elements that are a floor tile.
		var count = Generator.countElements(data, Map.FLOOR);

		// Our open list.
		var open = new Heap(Heap.type.MIN, count);
		var closed = [];
		var parent = [];

		// Arrays to contain our g and h cost.
		var g_cost = [];
		var h_cost = [];

		// Function to check if a move is legal.
		function isLegal(row, col)
		{
			var index = getIndex(data, row, col);

			for (var i = 0; i < closed.length; i++)
			{
				if (closed[i] == index)
					return false;
			}

			if (checkIndex(data, row) == 0 && 
				checkIndex(data, col) == 0)
				return true;

			return false;
		}

		// Function to calculate the G cost
		function calcGCost(r, c, parent)
		{
			var parentPos = getRowCol(data, parent);
			var cost = g_cost[parent];

			if (r == parentPos.row || c == parentPos.col)
				cost += STRAIGHT_COST;
			else
				cost += DIAGONAL_COST;

			return cost;
		}

		// Function to calculate the H cost
		function calcHCost(r, c, target)
		{
			return STRAIGHT_COST * (Math.abs(r - target.row) + Math.abs(c - target.col));
		}

		if (isLegal(start.row, start.col))
		{
			// Set the G cost of the start node to 0.
			g_cost[getIndex(data, start.row, start.col)] = 0;

			// Add the start node to the open list.
			open.addElement(getIndex(data, start.row, start.col), 0);
		}

		while (open.size() > 0)
		{
			var pivot = getRowCol(data, open.getRoot().ID);

			// Check to see if we just added our goal to the closed list.
			if (open.getRoot().ID == getIndex(data, target.row, target.col))
			{				
				// Reconstruct the path.
				var current, path = new Array();
				path.push(getIndex(data, target.row, target.col));
				while (path[path.length - 1] != getIndex(data, start.row, start.col))
				{
					current = path[path.length - 1];
					path.push(parent[current]);
				}
				return path;
			}

			// Add the ID of the root of the heap to the closed list.
			// console.log("Added " + open.getRoot().ID + " to the closed list");
			closed.push(open.removeRoot().ID);

			// Get all of the neighbor positions.
			for (var i = -1; i <= 1; i++)
			{
				for (var j = -1; j <= 1; j++)
				{
					if (i == 0 && j == 0)
						continue;

					var r = pivot.row + i;
					var c = pivot.col + j;
					var index = getIndex(data, r, c);

					// If the move is legal add it to the open list.
					if (isLegal(r, c))
					{
						if (open.hasID(index))
						{
							// Check for a smaller G cost.
							var newGCost = calcGCost(r, c, getIndex(data, pivot.row, pivot.col));
							if (newGCost < g_cost[index])
							{
								parent[index] = getIndex(data, pivot.row, pivot.col);
								g_cost[index] = newGCost;

								open.setElement(index, g_cost[index] + h_cost[index]);
							}
						}
						else
						{
							// Calculate the g and h cost for this node.
							g_cost[index] = calcGCost(r, c, getIndex(data, pivot.row, pivot.col));
							h_cost[index] = calcHCost(r, c, target);
							parent[index] = getIndex(data, pivot.row, pivot.col);
							
							open.addElement(index, g_cost[index] + h_cost[index]);
						}
					}
				}
			}
		}
		return [];
	};


	/**
	 * [getRandomData description]
	 * 
	 * @param  {[type]} size         [description]
	 * @param  {[type]} distribution [description]
	 * @param  {[type]} seed         [description]
	 * @return {[type]}              [description]
	 */
	Generator.getRandomData = function(size, distribution, options)
	{
		var options = options || {};
		var random = ('seed' in options) ? new Math.seedrandom(options.seed) : Math.random;
		var low = ('low' in options) ? options.low : 0;
		var high = ('high' in options) ? options.high : 1;

		var data = [];
		for (var i = 0; i < size; i++)
		{
			data[i] = [];
			for (var j = 0; j < size; j++)
			{
				data[i][j] = (distribution > 1 || random() < distribution) ? high : low;
			}
		}
		return data;
	};

	/**
	 * Gets a random row and column position of a 2D array that matches a given value.
	 * All matching elements are equally likely to be chosen.
	 * 
	 * @param  {Array}  data  	The 2D array of data.
	 * @param  {Number} value 	The value to search for.
	 * @param  {String} seed    The seed value for the random generator.
	 * @return {Object}       	An object containing a row and column. Null if no match is found.
	 */
	Generator.getRandomPosition = function(data, value, seed)
	{
		var random = (seed == null) ? Math.random : new Math.seedrandom(seed);
	    var elements = new Array();

	    for (var i = 0; i < data.length; i++)
		{
			for (var j = 0; j < data[i].length; j++)
			{
				if (data[i][j] == value)
				{
					elements.push({ row : i, col : j });
				}
			}
		}

		if (elements.length == 0)
		{
			return null;
		}
		else
		{
			var index = Math.floor(random() * (elements.length - 1));
			return elements[index];
		}
	};

	/**
	 * Counts all elements in the data that match the specified value.
	 * 
	 * @param  {Array}  data  	The 2D array of data.
	 * @param  {Number} value 	The value to search for.
	 * @return {Number}       	The number of elements in the data that match the value.
	 */
	Generator.countElements = function(data, value)
	{
		var count = 0;
	    for (var i = 0; i < data.length; i++)
		{
			for (var j = 0; j < data[i].length; j++)
			{
				if (data[i][j] == value)
				{
					count ++;
				}
			}
		}
		return count;
	}

	/**
	 * [applyCellAutomata description]
	 * 
	 * @param  {[type]} map     [description]
	 * @param  {[type]} born    [description]
	 * @param  {[type]} survive [description]
	 * @param  {[type]} options [description]
	 * @return {[type]}         [description]
	 */
	Generator.applyCellAutomata = function(map, born, survive, options)
	{
		var options = options || {};
		var passCount = options.passCount || 1;
		var neighborhoodSize = options.neighborhoodSize || 1;
		var live = ('live' in options) ? options.live : 1;
		var dead = ('dead' in options) ? options.dead : 0;

		function generateAt(map, row, col)
		{
			var neighborCount = 0;
			for (var i = -neighborhoodSize; i <= neighborhoodSize; i++)
			{
				for (var j = -neighborhoodSize; j <= neighborhoodSize; j++)
				{
					if (i == 0 && j == 0) continue;

					if (checkIndex(map, row + i) == 0 && checkIndex(map, col + j) == 0)
					{
						if (map[row + i][col + j] == live)
						{
							neighborCount ++;
						}
					}
				}
			}

			if (neighborCount >= born)
			{
				return live;
			}
			else if (neighborCount >= survive && map[row][col] == live)
			{
				return live;
			}
			else return dead;
		}

		var newMap = map;
		for (var n = 0; n < passCount; n++)
		{
			var tempMap = new Array(map.length);
			for (var i = 0; i < map.length; i++)
			{
				tempMap[i] = new Array(map[i].length);
				for (var j = 0; j < map[i].length; j++)
				{
					tempMap[i][j] = generateAt(newMap, i, j);
				}
			}
			newMap = tempMap;
		}
		return tempMap;
	};

	/**
	 * [floodFill description]
	 * 
	 * @param  {[type]} map         [description]
	 * @param  {[type]} row         [description]
	 * @param  {[type]} col         [description]
	 * @param  {[type]} target      [description]
	 * @param  {[type]} replacement [description]
	 * @param  {[type]} options     [description]
	 */
	Generator.floodFill = function(map, row, col, target, replacement, options)
	{
		var options = options || {};
		var spread = options.spread || 1;
		var decay = options.decay || 1;

		var random = options.random || (('seed' in options) ? new Math.seedrandom(options.seed) : Math.random);

		function floodFillRecursive(row, col, spread)
		{
			var element;

			if (checkIndex(map, row) == 0 && checkIndex(map, col) == 0)
			{
				element = map[row][col];
			}
			else
			{
				return 0;
			}
			
			if (element == replacement)
			{
				return 0;
			}
			else if (element == target)
			{
				map[row][col] = replacement;
				var count = 1;

				for (var i = -1; i <= 1; i++)
				{
					for (var j = -1; j <= 1; j++)
					{
						if (i == 0 && j == 0)
						{
							continue;
						}
						else if (spread == 1 || random() < spread)
						{
							count += floodFillRecursive(row + i, col + j, spread * decay);
						}
					}
				}
				return count;
			}
			else
			{
				return 0;
			}
		}

		return floodFillRecursive(row, col, spread);
	};

	/**
	 * [isolateMaxRegion description]
	 * 
	 * @param  {[type]} map      [description]
	 * @param  {[type]} positive [description]
	 * @param  {[type]} negative [description]
	 * @return {[type]}          [description]
	 */
	Generator.isolateMaxRegion = function(map, positive, negative)
	{
		if (negative >= positive)
			return null;
		
		// Flood fill each region with a different index while storing the count of each region
		var count = [];
		var regionIndex = positive + 1;
		for (var i = 0; i < map.length; i++)
		{
			for (var j = 0; j < map[i].length; j++)
			{
				if (map[i][j] == positive)
				{
					count[regionIndex] = Generator.floodFill(map, i, j, positive, regionIndex);
					regionIndex ++;
				}
			}
		}

		// Find the index of the region with the highest count
		var maxRegion = positive + 1;
		for (var i = positive + 1; i <= regionIndex; i++)
		{
			if (count[i] > count[maxRegion])
			{
				maxRegion = i;
			}
		}

		// Set all elements of the max region to positive and everything else to negative
		for (var i = 0; i < map.length; i++)
		{
			for (var j = 0; j < map[i].length; j++)
			{
				map[i][j] = (map[i][j] == maxRegion) ? positive : negative;
			}
		}
	};

	/**
	 * Prints a 2D array to the console.
	 * 
	 * @param  {Array} data 	The 2D array of data.
	 */
	Generator.print = function(data)
	{
		for (var i = 0; i < data.length; i++)
		{
			var str = "";
			for (var j = 0; j < data[i].length; j++)
			{
				if (data[i][j] == 0)
				{
					str += ". ";
				}
				else
				{
					str += data[i][j] + " ";
				}
			}
			console.log(str);
		}
	};

	return Generator;

})();
