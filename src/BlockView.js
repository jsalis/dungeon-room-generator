/**
 * @class 			BlockView
 * 
 * @author 			John Salis <jsalis@stetson.edu>
 * @description 	Builds geometry to represent a single element of map data.
 */
BlockView = (function() {

	var widthSegments = 3;
	var heightSegments = 5;

	var tipBlockGeometry = new TipBlockGeometry(1, 1, widthSegments, heightSegments);
	var cornerBlockGeometry = new CornerBlockGeometry(1, 1 , widthSegments, heightSegments);
	var sideBlockGeometry = new SideBlockGeometry(1, 1, widthSegments, heightSegments);

	/**
	 * Creates a block mesh for a single element in the map data. The geometry depends on the value 
	 * of the element and its immediate neighbors.
	 * 
	 * @param  {Array} 		data 	A 2D array of map data.
	 * @param  {Integer} 	row 	The row position of the block.
	 * @param  {Integer} 	col 	The column position of the block.
	 */
	function BlockView(data, row, col)
	{
		this.geometry = null;
		this.matrix = null;

		var center = exists(data, row, col, Map.FLOOR);

		if (!center) 
			return;

		var north = exists(data, row - 1, col, Map.FLOOR);
		var south = exists(data, row + 1, col, Map.FLOOR);
		var west = exists(data, row, col - 1, Map.FLOOR);
		var east = exists(data, row, col + 1, Map.FLOOR);

		if (north && south && west && east) 
			return;

		// Set flags for the four possible tip configurations
		var nn = north && !west && !south && !east;
		var ss = !north && !west && south && !east;
		var ww = !north && west && !south && !east;
		var ee = !north && !west && !south && east;

		var tip = nn || ss || ww || ee;

		// Set flags for the four possible corner configurations
		var nw = north && west && !south && !east;
		var ne = north && east && !south && !west;
		var sw = south && west && !north && !east;
		var se = south && east && !north && !west;

		var corner = nw || ne || sw || se;

		// Build block geometry
		if (tip)
		{
			this.geometry = tipBlockGeometry;
		}
		else if (corner)
		{
			this.geometry = cornerBlockGeometry;
		}
		else
		{
			this.geometry = new THREE.Geometry();

			if (!north)
			{
				var matrix = new THREE.Matrix4().makeRotationY(Math.PI);
				this.geometry.merge(sideBlockGeometry, matrix);
			}
			if (!east)
			{
				var matrix = new THREE.Matrix4().makeRotationY(Math.PI / 2);
				this.geometry.merge(sideBlockGeometry, matrix);
			}
			if (!west)
			{
				var matrix = new THREE.Matrix4().makeRotationY(Math.PI / -2);
				this.geometry.merge(sideBlockGeometry, matrix);
			}
			if (!south)
			{
				this.geometry.merge(sideBlockGeometry);
			}
		}

		// Set material index for faces
		for (var n = 0; n < this.geometry.faces.length; n++)
		{
			this.geometry.faces[n].materialIndex = data[row][col];
		}

		// Set block rotation
		this.matrix = new THREE.Matrix4();

		if (ss || se)
		{
			this.matrix.makeRotationY(Math.PI);
		}
		else if (ww || sw)
		{
			this.matrix.makeRotationY(Math.PI / 2);
		}
		else if (ee || ne)
		{
			this.matrix.makeRotationY(Math.PI / -2);
		}
	}

	function exists(data, row, col, val)
	{
		return (data[row] !== undefined && data[row][col] !== null && data[row][col] !== val);
	}

	return BlockView;

})();
