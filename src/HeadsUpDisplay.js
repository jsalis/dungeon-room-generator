/**
 * Class:       HeadsUpDisplay
 * Author:      John Salis
 * Purpose:     
 * 
 * Date:        August 6, 2014
 */

HeadsUpDisplay.prototype.constructor = HeadsUpDisplay;

function HeadsUpDisplay(renderer)
{
	this.scene = new THREE.Scene();

	// Create camera and add it to the scene
	var camRatio = window.innerWidth / window.innerHeight;
    var hudWidth = 50;
    var hudHeight = hudWidth / camRatio;
    this.camera = new THREE.OrthographicCamera(-hudWidth, hudWidth, hudHeight, -hudHeight, 1, hudWidth);
    this.camera.position.z = 2;
    this.scene.add(this.camera);

    this.render = function()
	{
		renderer.autoClear = false;
		renderer.render(this.scene, this.camera);
		renderer.autoClear = true;
	};
}
