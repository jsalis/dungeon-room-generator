/**
 * @class 		DungeonGenerator
 * 
 * @author 		John Salis
 * @description Static functions to modify a 2D array in a way that procedurally builds a dungeon.
 */
DungeonGenerator = {};

DungeonGenerator.generate = function(size, wallValue, floorValue, seed)
{
	var ROOM = 0;
	var HALL = 1;

	var ROOM_RATIO = 0.6;

	var MAX_ROOM_SIZE = 12;
	var MIN_ROOM_SIZE = 6;

	var MAX_HALL_SIZE = 9;
	var MIN_HALL_SIZE = 5;

	var TOLERANCE = 5;

	var random = (seed === null) ? Math.random : new Math.seedrandom(seed);

	var data = Generator.getRandomData(size, 1, { high: wallValue });

	var mid = Math.round(size / 2);

	var growthPoints = [{ row: mid, col: mid, rowDir: 0, colDir: 1, source: HALL }];

	function inRange(row, col)
	{
		return (row > 0 && row < size - 1 && col > 0 && col < size - 1);
	}

	function isAvailable(row, col)
	{
		for (var i = -1; i <= 1; i++)
		{
			for (var j = -1; j <= 1; j++)
			{
				var r = row + i;
				var c = col + j;

				if (!inRange(r, c) || data[r][c] == floorValue)
					return false;
			}
		}
		return true;
	}

	function dist(r, c, rr, cc)
	{
		return Math.sqrt(Math.pow(r - rr, 2) + Math.pow(c - cc, 2));
	}

	function createRoom(point)
	{
		var range = MAX_ROOM_SIZE - MIN_ROOM_SIZE;
		var height = MIN_ROOM_SIZE + Math.round(Math.random() * range);
		var width = MIN_ROOM_SIZE + Math.round(Math.random() * range);

		var source = { rowDir: point.rowDir, colDir: point.colDir };
		var offset = { row: 0, col: 0 };

		if (point.rowDir == 0)
		{
			point.rowDir = 1;
			offset.row = Math.round(Math.random() * height);
		}

		if (point.colDir == 0)
		{
			point.colDir = 1;
			offset.col = Math.round(Math.random() * width);
		}

		var floors = [];

		for (var i = 0; i < height; i++)
		{
			for (var j = 0; j < width; j++)
			{
				var row = point.row - offset.row + (i * point.rowDir);
				var col = point.col - offset.col + (j * point.colDir);

				var nearPoint = dist(row, col, point.row, point.col) < 2;

				if (!isAvailable(row, col) && !nearPoint)
				{
					return false;
				}
				else
				{
					floors.push({ row: row, col: col });
				}
			}
		}

		for (var i = 0; i < floors.length; i++)
		{
			var f = floors[i];
			data[f.row][f.col] = floorValue;
		}

		var halfHeight = (height - 1) / 2;
		var halfWidth = (width - 1) / 2;

		var midRow = point.row - offset.row + (halfHeight * point.rowDir);
		var midCol = point.col - offset.col + (halfWidth * point.colDir);

		for (var r = -1; r <= 1; r++)
		{
			for (var c = -1; c <= 1; c++)
			{
				var isCardinalDir = (r == 0 ^ c == 0);
				var isSourceDir = (r == -source.rowDir && c == -source.colDir);

				if (isCardinalDir && !isSourceDir)
				{
					var row = (r == 0) ? Math.floor(midRow) : midRow + ((halfHeight + 1) * r);
					var col = (c == 0) ? Math.floor(midCol) : midCol + ((halfWidth + 1) * c);

					if (inRange(row, col))
					{
						growthPoints.push({
							row: row,
							col: col,
							rowDir: r,
							colDir: c,
							source: ROOM
						});
					}
				}
			}
		}

		return true;
	}

	function createHall(point)
	{
		var range = MAX_HALL_SIZE - MIN_HALL_SIZE;
		var length = MIN_HALL_SIZE + Math.round(Math.random() * range);

		if (point.source == HALL)
		{
			if (point.rowDir == 0)
			{
				point.rowDir = (Math.random() < 0.5) ? 1 : -1;
				point.colDir = 0;
			}
			else if (point.colDir == 0)
			{
				point.colDir = (Math.random() < 0.5) ? 1 : -1;
				point.rowDir = 0;
			}
		}

		var floors = [];

		for (var i = 0; i < length; i++)
		{
			var row = point.row + (i * point.rowDir);
			var col = point.col + (i * point.colDir);

			var isPoint = (row == point.row && col == point.col);

			var nearPoint = dist(row, col, point.row, point.col) < 2;
			var lastPoint = (i == length - 1);

			if (!isAvailable(row, col) && !nearPoint && !lastPoint)
			{
				return false;
			}
			else
			{
				floors.push({ row: row, col: col });
			}
		}

		for (var i = 0; i < floors.length; i++)
		{
			var f = floors[i];
			data[f.row][f.col] = floorValue;
		}

		var row = (point.rowDir == 0) ? point.row : point.row + (length * point.rowDir);
		var col = (point.colDir == 0) ? point.col : point.col + (length * point.colDir);

		if (inRange(row, col))
		{
			growthPoints.push({
				row: row,
				col: col,
				rowDir: point.rowDir,
				colDir: point.colDir,
				source: HALL
			});
		}

		return true;
	}

	while (growthPoints.length > 0)
	{
		// Get a random growth point from the list
		var index = Math.floor(Math.random() * growthPoints.length);
		var point = growthPoints.splice(index, 1)[0];

		var feature;
		var errorCount = 0;

		// Choose the next feature based on the source of the point
		if (point.source == ROOM)
		{
			feature = HALL;
		}
		else
		{
			feature = (Math.random() < ROOM_RATIO) ? ROOM : HALL;
		}

		// Attempt to create a new feature
		if (feature == ROOM)
		{
			while (!createRoom(point) && errorCount < TOLERANCE)
			{
				errorCount ++;
			}
		}
		else if (feature == HALL)
		{
			while (!createHall(point) && errorCount < TOLERANCE)
			{
				errorCount ++;
			}
		}
	}

	return data;
};
